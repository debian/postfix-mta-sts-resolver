postfix-mta-sts-resolver (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.
  * systemd service: wait until PostgreSQL is started.

 -- Benjamin Hof <vexel@vexel.net>  Mon, 10 Mar 2025 21:31:19 +0100

postfix-mta-sts-resolver (1.4.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #1089790)
  * d/control: Upstream switches from aioredis to redis-py, reflect that in
    dependencies. (Closes: #1040331)
  * d/control: Add asyncpg dependency.
  * d/watch: Restore compatibility to Github.
  * d/copyright: bump years.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Benjamin Hof <vexel@vexel.net>  Sat, 29 Jun 2024 22:14:53 +0200

postfix-mta-sts-resolver (1.1.2-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to move systemd units into /usr.

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 27 May 2024 00:37:01 +0200

postfix-mta-sts-resolver (1.1.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:37:56 +0200

postfix-mta-sts-resolver (1.1.2-1) unstable; urgency=medium

  * New upstream release. (Closes: #996048)
  * Debian CI: testsuite now needs machine, not container.
  * Update salsa-ci.yml to currently recommended configuration.
  * Bump Standards-Version to 4.6.0.1, no changes needed.
  * Update copyright years.

 -- Benjamin Hof <vexel@vexel.net>  Sun, 30 Jan 2022 20:13:57 +0100

postfix-mta-sts-resolver (1.0.0-4) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.  (Thanks to the Janitor Team.)

  [ Benjamin Hof ]
  * Bump Standards-Version to 4.5.1, no changes needed.

 -- Benjamin Hof <vexel@vexel.net>  Sat, 05 Dec 2020 20:24:01 +0100

postfix-mta-sts-resolver (1.0.0-3) unstable; urgency=medium

  * Service file: start after redis. (Closes: #968594)

 -- Benjamin Hof <vexel@vexel.net>  Tue, 18 Aug 2020 20:46:41 +0200

postfix-mta-sts-resolver (1.0.0-2) unstable; urgency=medium

  * Fixed bug in init script that caused package installation to fail under
    sysvinit. Reported and fix suggested by Stephan Seitz.

 -- Benjamin Hof <vexel@vexel.net>  Sun, 26 Jul 2020 22:33:12 +0200

postfix-mta-sts-resolver (1.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Add writable Unix domain socket location to service file.
  * Bump debhelper-compat level to 13.

 -- Benjamin Hof <vexel@vexel.net>  Wed, 17 Jun 2020 20:59:20 +0200

postfix-mta-sts-resolver (0.8.2-1) unstable; urgency=medium

  * New upstream release.

 -- Benjamin Hof <vexel@vexel.net>  Tue, 14 Apr 2020 20:06:25 +0200

postfix-mta-sts-resolver (0.8.1-1) unstable; urgency=medium

  * New upstream release.

 -- Benjamin Hof <vexel@vexel.net>  Sat, 04 Apr 2020 11:45:39 +0200

postfix-mta-sts-resolver (0.8.0-1) unstable; urgency=medium

  * New upstream release.

 -- Benjamin Hof <vexel@vexel.net>  Tue, 24 Mar 2020 19:09:05 +0100

postfix-mta-sts-resolver (0.7.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes needed.

 -- Benjamin Hof <vexel@vexel.net>  Sun, 26 Jan 2020 16:25:29 +0100

postfix-mta-sts-resolver (0.7.3-1) unstable; urgency=medium

  * New upstream release.
  * Add autopkgtests reusing upstream functional tests.
  * Drop dependency on pynetstring, it is no longer needed.
  * Update copyright to 2020.

 -- Benjamin Hof <vexel@vexel.net>  Sun, 12 Jan 2020 12:23:41 +0100

postfix-mta-sts-resolver (0.7.0-1) unstable; urgency=medium

  * New upstream version.
  * Add rules-requires-root to control file.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Add upstream-contact to copyright file.

 -- Benjamin Hof <vexel@vexel.net>  Mon, 18 Nov 2019 20:43:13 +0100

postfix-mta-sts-resolver (0.6.0-1) unstable; urgency=medium

  * Initial packaging. (Closes: #917366)

 -- Benjamin Hof <vexel@vexel.net>  Sat, 17 Aug 2019 22:47:08 +0200
